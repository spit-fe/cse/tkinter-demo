import csv
from tkinter import messagebox
from tkinter import ttk
from tkinter import *


def option_button_pressed(gui, button, questions):
    print("Button text =", button.cget("text")[3:])
    print("Correct Answer is: ", questions[gui.question_number][5])
    if button.cget("text")[3:].strip() == questions[gui.question_number][5].strip():
        print("Correct Answer!")
        messagebox.showinfo("Correct!", "Your answer is correct.")
        gui.score += 1

        gui.quiz_description = f"{gui.score}/{len(questions)}"
        gui.score_label.config(text=gui.quiz_description)
        # gui.question_number += 1
        question_update(gui, questions)
    else:
        messagebox.showerror("Wrong!", "Your answer is wrong.")
        question_update(gui, questions)


def question_update(gui, questions):
    gui.question_number += 1
    if gui.question_number > len(questions)-1:
        messagebox.showinfo("Quiz Finished!", f"You scored {gui.score}/{len(questions)}.")
        gui.destroy()

    print(f"Update GUI with new question {gui.question_number}")
    gui.quiz_list.config(state='normal')
    gui.quiz_list.delete("1.0", END)
    gui.quiz_list.insert("1.0", questions[gui.question_number][0])
    gui.quiz_list.config(state='disabled')
    gui.option_a_button.config(
        text=f"A. {questions[gui.question_number][1]}",
    )
    gui.option_b_button.config(
        text=f"B. {questions[gui.question_number][2]}",
    )
    gui.option_c_button.config(
        text=f"C. {questions[gui.question_number][3]}",
    )
    gui.option_d_button.config(
        text=f"D. {questions[gui.question_number][4]}",
    )
    


def load_quiz():
    rows = []
    with open("quiz.csv", "r") as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        for row in csvreader:
            rows.append(row)
    print(header)
    return rows


if __name__ == "__main__":
    load_quiz()
