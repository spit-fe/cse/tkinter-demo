import tkinter as tk
from tkinter import Menu

# Main Window
window = tk.Tk()
window.title("Sample Application")
window.geometry("250x250")
# Title label
title_label = tk.Label(master=window, text='Miles to Kilometers')
title_label.pack()
entry = tk.Entry()
entry.pack()
convert_button = tk.Button(text='Convert')
convert_button.pack()
result_label = tk.Label(master=window, text='Please provide input')
result_label.pack()

distance = entry.get()
print(distance)

"""
Menubar is an important part.
When an application contains a lot of functions,
you need to use menus to organize them for easier navigation.

Typically, you use a menu to group closely related operations.
For example, you can find the File menu in most text editors.

Tkinter natively supports menus.
It displays menus with the look-and-feel of the target platform that the program runs e.g., Windows, macOS, and Linux.
"""
# create a menubar
menubar = Menu(window)
window.config(menu=menubar)

# create a menu
file_menu = Menu(menubar, tearoff=False)

# add a menu item to the menu
file_menu.add_command(
    label='Exit',
    command=window.destroy
)

# add the File menu to the menubar
menubar.add_cascade(
    label="File",
    menu=file_menu
)

window.mainloop()