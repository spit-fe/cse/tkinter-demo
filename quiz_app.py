import os
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import quiz_utils as qu


class QuizGUI(Tk):
    def __init__(self, title, questions):
        """
        Initialises the UI with title and logo.
        """
        Tk.__init__(self)

        # Set title
        self.title(title)
        self.option_add("*Dialog.msg.font", "Arial 12")
        self.score = 0
        self.question_number = 0

        # Disable minimize/maximize
        self.resizable(0, 0)

        # TTK styling information
        self.s = ttk.Style()
        self.s.configure("primary.TButton", font=("Arial", 10))
        self.s.configure("main.TLabel", font=("Arial", 20, "bold"))

        # Add logo frame
        self.logo_frame = ttk.Frame(self)
        self.logo_frame.pack(fill="x")

        # Build interface
        self.build_interface()

    def build_interface(self):
        """
        Builds the main interface.
        """
        # Tabbed interface
        self.agent_notebook = ttk.Notebook(self)
        self.agent_notebook.pack()

        # Status bar
        self.statusbar = ttk.Label(
            self, text="Status bar", borderwidth=1, relief="groove", anchor="w"
        )
        self.statusbar.pack(fill="x")
        self.quiz_interface_builder()
        self.about_interface_builder()
        self.chat_interface_builder()
        self.ticketing_interface_builder()

    # Ticketing interface builder
    def ticketing_interface_builder(self):
        """
        Builds elements in the ticketing tab
        """
        self.ticket_frame = ttk.Frame(self.agent_notebook)
        self.ticket_frame.pack(fill="both")

        self.ticket_frame_desc = ttk.LabelFrame(self.ticket_frame, text="Description")
        self.ticket_frame_desc.pack(fill="both", pady=5)

        self.ticketing_tab_description = "You can request support by opening support tickets. Your issue will be taken care of by a technician."
        ttk.Label(
            self.ticket_frame_desc,
            text=self.ticketing_tab_description,
            font=("Arial", 10),
            wraplength=400,
            justify="left",
        ).pack(padx=10, pady=5, side="left")

        self.ticket_form_frame = ttk.LabelFrame(self.ticket_frame, text="Create ticket")
        self.ticket_form_frame.pack(fill="both", pady=5, expand="yes")

        self.customer_detail_frame = ttk.Frame(self.ticket_form_frame)
        self.customer_detail_frame.pack(fill="both")

        self.customer_name_frame = ttk.Frame(self.customer_detail_frame)
        self.customer_name_frame.pack(side="left")

        ttk.Label(self.customer_name_frame, text="Name", font=("Arial", 10)).pack(
            padx=10, anchor="w"
        )

        self.issuer_name = ttk.Entry(self.customer_name_frame, font=("Arial", 12))
        self.issuer_name.pack(padx=5)

        self.customer_email_frame = ttk.Frame(self.customer_detail_frame)
        self.customer_email_frame.pack(side="left")

        ttk.Label(self.customer_email_frame, text="E-mail", font=("Arial", 10)).pack(
            padx=10, anchor="w"
        )
        self.issuer_email = ttk.Entry(self.customer_email_frame, font=("Arial", 12))
        self.issuer_email.pack(padx=5)

        ttk.Label(self.ticket_form_frame, text="Issue", font=("Arial", 10)).pack(
            fill="x", padx=10, pady=(10, 0)
        )
        self.issue_subject = ttk.Entry(self.ticket_form_frame, font=("Arial", 12))
        self.issue_subject.pack(fill="x", padx=5)

        ttk.Label(self.ticket_form_frame, text="Description", font=("Arial", 10)).pack(
            fill="x", padx=10, pady=(10, 0)
        )
        self.issue_description = Text(
            self.ticket_form_frame,
            font=("Arial", 12),
            bg="white",
            fg="black",
            width=20,
            height=4,
            wrap="word",
        )
        self.issue_description.pack(fill="x", padx=5)

        self.selected_priority = StringVar()
        self.priority_options = [
            "Urgent",
            "High",
            "Normal",
            "Low",
        ]
        ttk.Label(self.ticket_form_frame, text="Priority", font=("Arial", 10)).pack(
            fill="x", padx=10, pady=(10, 0)
        )
        self.priority_dropdown = ttk.OptionMenu(
            self.ticket_form_frame,
            self.selected_priority,
            self.priority_options[2],
            *self.priority_options
        )
        self.priority_dropdown.configure(width=15)
        self.priority_dropdown.pack(padx=10, anchor="w")

        self.ticket_form_buttons = ttk.Frame(self.ticket_form_frame)
        self.ticket_form_buttons.pack(fill="x", padx=10, pady=(10, 0))
        # Add to notebook
        self.agent_notebook.add(self.ticket_frame, text="Ticketing")

        # Clear button
        self.clear_ticket_form_button = ttk.Button(
            self.ticket_form_buttons,
            text="Clear",
            style="primary.TButton",
            # command = lambda: clear()
        )
        self.clear_ticket_form_button.pack(side="right")

        # Open ticket button
        self.open_ticket_button = ttk.Button(
            self.ticket_form_buttons,
            text="Open Ticket",
            style="primary.TButton",
            # command = lambda: submit(
            #     issuer_name.get(),
            #     issue_subject.get(),
            #     issue_description.get("1.0",END)
            #     )
        )
        self.open_ticket_button.pack(padx=5, side="right")

    # Chat interface builder
    def chat_interface_builder(self):
        self.chat_frame = ttk.Frame(self.agent_notebook)
        self.chat_frame.pack(fill="both", expand=1)

        # Chat tab content

        self.chat_frame_desc = ttk.LabelFrame(self.chat_frame, text="Description")
        self.chat_frame_desc.pack(fill="both", pady=5)

        self.chat_description = (
            "Chat with technicians to resolve issues or clear doubts."
        )
        ttk.Label(
            self.chat_frame_desc,
            text=self.chat_description,
            font=("Arial", 10),
            wraplength=400,
            justify="left",
        ).pack(padx=10, pady=5, side="left")

        # ttk.Label(self.chat_frame, text=self.chat_tab_description, font=('Arial', 10), wraplength=500, justify='left').place(relx=0.01, rely=0.01)

        self.chat_area = ttk.LabelFrame(self.chat_frame, text="Conversation")
        # self.chat_area.place(relx=0.0, rely=0.1, relwidth=1, relheight=0.7)
        self.chat_area.pack(fill="both", pady=5)

        self.conversation_list = Text(
            self.chat_area,
            bg="white",
            fg="black",
            state="disabled",
            font=("Arial", 12),
            height=15,
            width=45,
            wrap="word",
        )

        # self.conversation_list.place(relwidth=0.95, relheight=1)
        self.conversation_list.pack(fill="y", side="left")

        self.conversation_list_scroll = ttk.Scrollbar(
            self.chat_area, command=self.conversation_list.yview
        )
        self.conversation_list_scroll.pack(fill="y", side="left")

        self.conversation_list.config(yscrollcommand=self.conversation_list_scroll.set)

        self.chat_input_frame = ttk.LabelFrame(self.chat_frame, text="Your message")
        self.chat_input_frame.pack(fill="both", pady=5)

        self.chat_input = Text(
            self.chat_input_frame,
            font=("Arial", 12),
            bg="white",
            fg="black",
            height=2,
            width=37,
            wrap="word",
        )
        self.chat_input.pack(fill="x", side="left", pady=(0, 5))

        self.send_chat_message_button = ttk.Button(
            self.chat_input_frame,
            text="Send",
            style="primary.TButton",
            # command = lambda: send_chat_message(
            #     'you',
            #     chat_input.get("1.0",END)
            #     )
        )
        self.send_chat_message_button.pack(
            fill="both", side="left", padx=(5, 0), pady=(0, 5)
        )

        # Add to notebook
        self.agent_notebook.add(self.chat_frame, text="Chat")

    # Quiz interface builder
    def quiz_interface_builder(self):
        self.quiz_frame = ttk.Frame(self.agent_notebook)
        self.quiz_frame.pack(fill="both", expand=1)

        # Quiz tab content

        self.quiz_frame_desc = ttk.LabelFrame(self.quiz_frame, text="Your Score")
        self.quiz_frame_desc.pack(fill="both", pady=5)

        self.quiz_description = f"{self.score}/{len(questions)}"
        self.score_label = ttk.Label(
            self.quiz_frame_desc,
            text=self.quiz_description,
            font=("Arial", 20),
            wraplength=400,
            justify="left",
        )
        
        self.score_label.pack(padx=10, pady=5, side="left")


        self.quiz_area = ttk.LabelFrame(self.quiz_frame, text="Question")
        self.quiz_area.pack(fill="both", pady=5)

        self.quiz_list = Text(
            self.quiz_area,
            bg="white",
            fg="black",
            # state="disabled",
            font=("Arial", 12),
            height=6,
            width=45,
            wrap="word",
        )
        self.quiz_list.insert("1.0",questions[0][0])
        self.quiz_list.pack(fill="y", side="left")
        self.quiz_list.config(state='disabled')

        self.quiz_list_scroll = ttk.Scrollbar(
            self.quiz_area, command=self.quiz_list.yview
        )
        self.quiz_list_scroll.pack(fill="y", side="left")

        self.quiz_list.config(yscrollcommand=self.quiz_list_scroll.set)

        # Options
        self.option_frame = ttk.Frame(self.quiz_frame)
        self.option_frame.pack(fill="both", pady=5)
        self.option_frame_desc = ttk.LabelFrame(
            self.option_frame, text="Options (select one)"
        )
        self.option_frame_desc.pack(fill="both", pady=5)

        # Button style
        option_style = ttk.Style()
        option_style.configure(
            "W.TButton",
            font=(
                "Arial",
                12,
            ),
            anchor=W,
        )

        self.option_a_button = ttk.Button(
            self.option_frame, text="Option A", style="W.TButton"
        )
        self.option_a_button.pack(fill="both")
        self.option_a_button.config(
            text=f"A. {questions[0][1]}",
            command=lambda button=self.option_a_button: qu.option_button_pressed(self, button, questions,),
        )

        self.option_b_button = ttk.Button(
            self.option_frame, text="Option B", style="W.TButton"
        )
        self.option_b_button.pack(fill="both")
        self.option_b_button.config(
            text=f"B. {questions[0][2]}",
            command=lambda button=self.option_b_button: qu.option_button_pressed(self, button, questions,),
        )

        self.option_c_button = ttk.Button(
            self.option_frame, text="Option C", style="W.TButton"
        )
        self.option_c_button.pack(fill="both")
        self.option_c_button.config(
            text=f"C. {questions[0][3]}",
            command=lambda button=self.option_c_button: qu.option_button_pressed(self, button, questions,),
        )

        self.option_d_button = ttk.Button(
            self.option_frame, text="Option D", style="W.TButton"
        )
        self.option_d_button.pack(fill="both")
        self.option_d_button.config(
            text=f"D. {questions[0][4]}",
            command=lambda button=self.option_d_button: qu.option_button_pressed(self, button, questions,),
        )

        # Add to notebook
        self.agent_notebook.add(self.quiz_frame, text="Quiz")

    # About interface builder
    def about_interface_builder(self):
        # self.about_frame = ttk.Frame(self.agent_notebook, width=500, height=500)
        self.about_frame = ttk.Frame(self.agent_notebook)
        self.about_frame.pack(fill="both")

        # About tab content
        self.about_frame_desc = ttk.LabelFrame(self.about_frame, text="Description")
        self.about_frame_desc.pack(fill="both", pady=5)

        self.about_tab_description = "This Quiz app is developed by Vipul Kushwaha."
        ttk.Label(
            self.about_frame_desc,
            text=self.about_tab_description,
            font=("Arial", 10),
            wraplength=400,
            justify="left",
        ).pack(padx=10, pady=5, side="left")

        self.system_detail_frame = ttk.LabelFrame(
            self.about_frame, text="System details"
        )
        self.system_detail_frame.pack(fill="both", pady=5, expand="yes")

        self.system_description = Text(
            self.system_detail_frame,
            font=("Arial", 10),
            bg="white",
            fg="black",
            width=20,
            height=16,
            wrap="word",
        )
        self.system_description.pack(fill="x")

        # Add to notebook
        self.agent_notebook.add(self.about_frame, text="About")


if __name__ == "__main__":
    questions = qu.load_quiz()
    agent_gui = QuizGUI("MyQuiz", questions)
    agent_gui.eval("tk::PlaceWindow . center")
    agent_gui.mainloop()
