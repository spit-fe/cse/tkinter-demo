from tkinter import *
from tkinter import ttk
from tkinter.font import Font


class UnitConverterGUI(Tk):
    def convert(self):
        distance = float(self.distance_entry.get())
        self.result_label.config(text=f"{round(distance*1.6, 3)}")

    def __init__(self, title):
        Tk.__init__(self)
        self.title(title)
        self.resizable(0, 0)

        self.main_frame = ttk.Frame(
            padding=10,
        )
        self.main_frame.pack()
        self.geometry("300x300")
        self.entry_label = ttk.Label(
            self.main_frame,
            text="Enter Distance in Miles",
        )
        self.entry_label.pack()
        self.distance_entry = ttk.Entry(
            self.main_frame,
            font=(
                "Arial",
                20,
            ),
        )
        self.distance_entry.pack()

        style = ttk.Style()
        style.configure(
            "W.TButton",
            font=(
                "Arial",
                12,
            ),
        )
        convert_button = ttk.Button(
            self.main_frame,
            text="Convert",
            style="W.TButton",
            command=lambda: self.convert(),
        )
        convert_button.pack()

        self.result_label = ttk.Label(
            self.main_frame,
            text="Distance in Kilometers",
        )
        self.result_label.pack()

        self.result_label = ttk.Label(
            self.main_frame,
            text="...",
            font=(
                "Arial",
                12,
            ),
        )
        self.result_label.pack()

        self.widget_frame = ttk.Frame(
            self.main_frame,
            padding=10,
        )

        self.widget_frame.pack()
        self.spinbox = Spinbox(
            self.widget_frame,
            from_=0,
            to=10,
            width=10,
            font=Font(size=12, weight="bold"),
        )
        self.spinbox.pack()

        # Creating Menubar
        self.menubar = Menu(self)

        # Adding File Menu and commands
        self.file = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="File", menu=self.file)
        self.file.add_command(label="New File", command=None)
        self.file.add_command(label="Open...", command=None)
        self.file.add_command(label="Save", command=None)
        self.file.add_separator()
        self.file.add_command(label="Exit", command=self.destroy)
        self.config(menu=self.menubar)


if __name__ == "__main__":
    gui = UnitConverterGUI("My Converter")
    gui.eval("tk::PlaceWindow . center")
    gui.mainloop()
